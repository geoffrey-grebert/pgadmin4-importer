#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <Logger.h>
#include <dbmanager.h>
#include <QSqlQueryModel>

#include "pluginloader.h"
#include "plugin.h"

MainWindow::MainWindow(Options &options, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _options(options)
{
    ui->setupUi(this);
    _textEditAppender = new TextEditAppender(this);
    cuteLogger->registerAppender(_textEditAppender);

    connect(_textEditAppender, SIGNAL(newLog(QString)),ui->teLog, SLOT(append(QString)));
}

MainWindow::~MainWindow()
{
    if(_textEditAppender)
    {
        disconnect(_textEditAppender);
        cuteLogger->removeAppender(_textEditAppender);
        _textEditAppender->deleteLater();
    }

    delete ui;
}

void MainWindow::on_action_Sortie_triggered()
{
    this->close();
}

void MainWindow::on_actionDepuis_un_fichier_reg_triggered()
{
    QString qsRegPath = QFileDialog::getOpenFileName(
                this,
                "Sélectionner un fichier .reg",
                QDir::homePath() + "/Downloads/",
                "Fichiers reg (*.reg)",
                0,
                QFileDialog::ReadOnly);

    LOG_DEBUG() << qsRegPath;

    this->_workInProgress();
}

void MainWindow::on_action_A_propos_triggered()
{
    QMessageBox::information(
                this,
                "A propos ...",
                QString("Version: 0.0.1") + "\n\n" + "C'est nous qui l'a fait!",
                QMessageBox::Ok);

}

void MainWindow::on_action_Mettre_jour_triggered()
{
    this->_workInProgress();
}

void MainWindow::_workInProgress()
{
    QMessageBox::information(
                this,
                "TODO",
                QString("Non implémenté"),
                QMessageBox::Ok);
}

Plugin *MainWindow::_getPlugin(const QString &pluginName)
{
    foreach(Plugin* plugin, _pluginList)
    {
        QString name = plugin->name();
        if(name == pluginName)
        {
            return plugin;
        }
    }

    return 0;
}

QSqlRelationalTableModel  * MainWindow::loadServers(const QString &path)
{
    DbManager dbMgr(path);

    QSqlRelationalTableModel *model = new QSqlRelationalTableModel;
    model->setTable("server");
    model->setRelation(2,  QSqlRelation("servergroup", "id", "name"));
    model->setHeaderData(2, Qt::Horizontal, QObject::tr("Group"));

    model->select();

    return model;
}

void MainWindow::on_actionDepuis_la_base_de_registre_triggered()
{
    this->_workInProgress();
}

void MainWindow::loadPlugins(const QString& pluginDir)
{
    _pluginList = PluginLoader::pluginByDir<Plugin>(pluginDir);

    foreach(Plugin* plugin, _pluginList)
    {
        QString pluginName = plugin->name();
        LOG_WARNING() << "Plugin inconnu : " << pluginName;

        connect(plugin,SIGNAL(logInfo(QString,QString)),this,SLOT(logInfo(QString,QString)));
        connect(plugin,SIGNAL(logWarning(QString,QString)),this,SLOT(logWarning(QString,QString)));
        connect(plugin,SIGNAL(logError(QString,QString)),this,SLOT(logError(QString,QString)));
        connect(plugin,SIGNAL(logDebug(QString,QString)),this,SLOT(logDebug(QString,QString)));
        connect(plugin,SIGNAL(updateProgress(int)),this,SLOT(updateProgress(int)));
        connect(plugin,SIGNAL(processFinished(QString,QStringList)),this,SLOT(processFinished(QString,QStringList)));

        LOG_INFO() << pluginName << " v" << plugin->version() << " Chargé";
    }
}

void MainWindow::unloadPlugins(const QString& pluginDir)
{
    PluginLoader::unloadByDir(pluginDir);
    _pluginList.clear();
}

void MainWindow::logInfo(const QString &pluginName, const QString &message)
{
    LOG_INFO() << pluginName << ": " << message;
}

void MainWindow::logWarning(const QString &pluginName, const QString &message)
{
    LOG_WARNING() << pluginName << ": " << message;
}

void MainWindow::logError(const QString &pluginName, const QString &message)
{
    LOG_ERROR() << pluginName << ": " << message;
}

void MainWindow::logDebug(const QString &pluginName, const QString &message)
{
    LOG_DEBUG() << pluginName << ": " << message;
}

void MainWindow::updateProgress(int val)
{
    LOG_INFO() << "Progress : " << val;
}

void MainWindow::processFinished(const QString &pluginName)
{
    Plugin* plugin = _getPlugin(pluginName);

    if(plugin)
    {
        int status= plugin->status();

        LOG_INFO() << "Status " << pluginName << ": " << status;

        if(0 == status)
        {
            QMessageBox::critical(this,tr("Echec"),tr("C'est la LOOSE, échec du traitement!"),QMessageBox::Ok);
        }
    }
}

void MainWindow::on_action_Charger_la_base_triggered()
{
    QString qsDb = QFileDialog::getOpenFileName(
                this,
                "Sélectionner la base PgAdmin",
                QDir::homePath() + "/AppData/Roaming/pgAdmin/",
                "Fichiers reg (*.db)",
                0,
                QFileDialog::ReadOnly);

    LOG_DEBUG() << qsDb;

    QSqlRelationalTableModel *model = loadServers(qsDb);

    ui->tvServers->setModel(model);
    ui->tvServers->setColumnHidden(0, true);
    ui->tvServers->setColumnHidden(1, true);
}
