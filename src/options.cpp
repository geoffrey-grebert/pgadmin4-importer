#include <options.h>
#include <QSettings>

void Options::readOptions(const QString &configurationFilePath)
{
    QSettings settings(configurationFilePath, QSettings::IniFormat);

    _logLevel = settings.value("LOG/logLevel", DEFAULT_LOG_LEVEL).toString();
}

QString Options::logLevel() const
{
    return _logLevel;
}

void Options::setLogLevel(const QString &logLevel)
{
    _logLevel = logLevel;
}
