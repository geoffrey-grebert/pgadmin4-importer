#include "mainwindow.h"
#include <QApplication>

#include <Logger.h>
#include <ConsoleAppender.h>
#include <FileAppender.h>
#include <options.h>

const QString CONF_FILENAME = "pgadmin-importer.conf";
const QString LOG_FILENAME = "pgadmin-importer.log";
const QString LOG_FORMAT = "[%{time}{yyyy-MM-dd HH:mm:ss.zzz}][%{type:-7}] <%{function}> %{message}\n";

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Options options;
    options.readOptions(QApplication::applicationDirPath() + "/" + CONF_FILENAME);

    ConsoleAppender* consoleAppender = new ConsoleAppender();
    consoleAppender->setDetailsLevel(options.logLevel());
    consoleAppender->setFormat(LOG_FORMAT);

    FileAppender* fileAppender = new FileAppender(QApplication::applicationDirPath() + "/" + LOG_FILENAME);
    fileAppender->setDetailsLevel(options.logLevel());
    fileAppender->setFormat(LOG_FORMAT);

    cuteLogger->registerAppender(consoleAppender);
    cuteLogger->registerAppender(fileAppender);

    LOG_INFO() << "***************";
    LOG_INFO() << "Démarrage du programme";

    MainWindow w(options);
    w.loadPlugins(QApplication::applicationDirPath() + "/plugins");
    w.show();

    int result = a.exec();

    w.unloadPlugins(QApplication::applicationDirPath() + "/plugins");

    LOG_INFO() << "Fin.";

    cuteLogger->removeAppender(consoleAppender);
    cuteLogger->removeAppender(fileAppender);

    return result;
}
