#include "texteditappender.h"

TextEditAppender::TextEditAppender(QObject *parent) : QObject(parent)
{

}

void TextEditAppender::append(const QDateTime &, Logger::LogLevel , const char *, int , const char *, const QString &, const QString &message)
{
    emit newLog(message + "\n");

}
