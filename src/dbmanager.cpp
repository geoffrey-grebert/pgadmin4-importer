#include "dbmanager.h"
#include "Logger.h"

DbManager::DbManager(const QString &path)
{
    _db = QSqlDatabase::addDatabase("QSQLITE");
    _db.setDatabaseName(path);

    if (!_db.open())
    {
        LOG_ERROR() << "Connection with database fail";
    }
    else
    {
        LOG_DEBUG() << "Database: connection ok";
    }
}

QSqlDatabase DbManager::db() const
{
    return _db;
}
