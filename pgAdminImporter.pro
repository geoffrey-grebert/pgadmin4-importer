#-------------------------------------------------
#
# Project created by QtCreator 2017-10-02T13:36:07
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pgAdminImporter
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += ./include ../common/include/CuteLogger dependencies/include
CONFIG(debug,debug|release):LIBS += -L../common/lib/CuteLogger/debug -Ldependencies/lib -lCuteLoggerd
CONFIG(release,debug|release):LIBS += -L../common/lib/CuteLogger/release -Ldependencies/lib -lCuteLogger

SOURCES += \
    src/main.cpp \
    src/mainwindow.cpp \
    src/texteditappender.cpp \
    src/options.cpp \
    src/dbmanager.cpp

HEADERS += \
    include/plugin.h \
    include/pluginloader.h \
    include/mainwindow.h \
    include/texteditappender.h \
    include/options.h \
    include/dbmanager.h

FORMS += \
        mainwindow.ui

GIT_VERSION = $$system(git describe --always --tags)
VERSION = $$GIT_VERSION
win32 {
    VERSION ~= s/-\d+-g[a-f0-9]{6,}//
}

DISTFILES += \
    pgadmin-importer.conf
