#ifndef PLUGIN_H
#define PLUGIN_H

#include <QtPlugin>
#include <QStringList>

/**
 * @brief The Plugin class
 */
class Plugin : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief ~Plugin dtor
     */
    virtual ~Plugin(){}

    /**
     * @brief name
     * @return the plugin name
     */
    virtual QString name() const =0;
    /**
     * @brief version
     * @return the plugin version
     */
    virtual QString version() const =0;
    /**
     * @brief status
     * @return the status of the last process
     */
    virtual int status() const { return _status; }

public slots:
    /**
     * @brief process launch the treatment
     * @param aArgList arguments list
     * @return  the status of the process
     */
    virtual int process(QStringList &aArgList) =0;

signals:
    /**
     * @brief logInfo log with info level
     * @param aPluginName Name of the plugin
     * @param aMessage Message to log
     */
    void logInfo(const QString& aPluginName, const QString& aMessage);
    /**
     * @brief logWarning log with warning level
     * @param aPluginName Name of the plugin
     * @param aMessage Message to log
     */
    void logWarning(const QString& aPluginName, const QString& aMessage);
    /**
     * @brief logError log with error level
     * @param aPluginName Name of the plugin
     * @param aMessage Message to log
     */
    void logError(const QString& aPluginName, const QString& aMessage);
    /**
     * @brief logDebug log with debug level
     * @param aPluginName Name of the plugin
     * @param aMessage Message to log
     */
    void logDebug(const QString& aPluginName, const QString& aMessage);
    /**
     * @brief updateProgress Update the progress value of the treatment
     * @param value
     */
    void updateProgress(int value);
    /**
     * @brief processFinished
     * @param aPluginName Name of the plugin
     * @param aList List of some data results
     */
    void processFinished(const QString& aPluginName, const QStringList &aList = QStringList());

protected:
    /**
     * @brief _status Status of the process
     */
    int _status;

};
Q_DECLARE_INTERFACE(Plugin, "Plugin")

#endif // PLUGIN_H
