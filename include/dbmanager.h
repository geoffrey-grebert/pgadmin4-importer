#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QSqlDatabase>

class DbManager
{
public:
    DbManager(const QString &path);
    QSqlDatabase db() const;

private:
    QSqlDatabase _db;
};

#endif // DBMANAGER_H
