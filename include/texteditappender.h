#ifndef TEXTEDITAPPENDER_H
#define TEXTEDITAPPENDER_H

#include <AbstractAppender.h>
#include <QObject>

class TextEditAppender : public QObject, public AbstractAppender
{
    Q_OBJECT
public:
    explicit TextEditAppender(QObject *parent = nullptr);

protected:
    void append (const QDateTime &, Logger::LogLevel, const char *, int, const char *, const QString &, const QString &message);

signals:
    void newLog(const QString &message);

public slots:
};

#endif // TEXTEDITAPPENDER_H
