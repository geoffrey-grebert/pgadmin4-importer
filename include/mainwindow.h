#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QSqlQueryModel>
#include <QSqlRelationalTableModel>

#include "options.h"
#include "plugin.h"
#include "texteditappender.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //! CTOR
    /*!
     * \brief MainWindow CTOR
     * \param parent
     */
    explicit MainWindow(Options &options, QWidget *parent = 0);
    //! DTOR
    ~MainWindow();

    /*!
     * \brief loadPlugins
     * \param pluginDir
     */
    void loadPlugins(const QString &pluginDir);
    /*!
     * \brief unloadPlugins
     * \param pluginDir
     */
    void unloadPlugins(const QString &pluginDir);

public slots:
    /*!
     * \brief logInfo
     * \param pluginName
     * \param aMessage
     */
    void logInfo(const QString &pluginName, const QString &message);

    /*!
     * \brief logWarning
     * \param pluginName
     * \param message
     */
    void logWarning(const QString &pluginName, const QString &message);

    /*!
     * \brief logError
     * \param pluginName
     * \param message
     */
    void logError(const QString &pluginName, const QString &message);

    /*!
     * \brief logDebug
     * \param pluginName
     * \param message
     */
    void logDebug(const QString &pluginName, const QString &message);

    /*!
     * \brief updateProgress
     * \param val
     */
    void updateProgress(int val);

    /*!
     * \brief processFinished
     * \param pluginName
     */
    void processFinished(const QString& pluginName);

private slots:
    void on_action_Sortie_triggered();

    void on_actionDepuis_un_fichier_reg_triggered();

    void on_action_A_propos_triggered();

    void on_action_Mettre_jour_triggered();

    void on_actionDepuis_la_base_de_registre_triggered();

    void on_action_Charger_la_base_triggered();

private:
    Ui::MainWindow *ui;
    QList<Plugin*> _pluginList;
    TextEditAppender *_textEditAppender;
    Options &_options;

private:
    void _workInProgress();
    Plugin *_getPlugin(const QString &pluginName);
    QSqlRelationalTableModel *loadServers(const QString &path);
};

#endif // MAINWINDOW_H
