#ifndef OPTIONS_H
#define OPTIONS_H

#include <QObject>

const QString DEFAULT_LOG_LEVEL = "Trace";

class Options :  public QObject
{
    Q_OBJECT

public:
    void readOptions(const QString &configurationFilePath);

    QString logLevel() const;
    void setLogLevel(const QString &logLevel);

private:
    QString _logLevel;


};

#endif // OPTIONS_H
