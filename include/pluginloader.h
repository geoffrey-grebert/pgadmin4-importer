#ifndef PLUGINLOADER_H
#define PLUGINLOADER_H

#include <QPluginLoader>
#include <QString>
#include <QList>
#include <QDir>

#include <exception>

/**
 * Plugin loader template
 */
namespace PluginLoader
{
template<typename T> T* pluginByName(const QString& aFileName)
{
    QPluginLoader loader(aFileName);
    QObject *plugin = loader.instance();
    if(0 == plugin)
    {
        throw std::exception();
    }

    return qobject_cast<T*>(plugin);
}

template<typename T> QList<T*> pluginByDir(const QDir& aDir)
{
    QList<T*> list;
    QDir pluginDir = QDir(aDir);

    // Filtrage des plugins par leur extension
    QStringList filters;
    filters << "*.dll" << "*.so";
    pluginDir.setNameFilters(filters);
    foreach(QString file, pluginDir.entryList(QDir::Files))
    {
        if(T* plugin = PluginLoader::pluginByName<T>(pluginDir.absoluteFilePath(file)))
        {
            list.push_back(plugin);
        }
    }

    return list;
}

void unloadByName(const QString& aFileName)
{
    QPluginLoader loader(aFileName);
    if(loader.isLoaded())
    {
        loader.unload();
    }
}

void unloadByDir(const QDir& aDir)
{
    QDir pluginDir = QDir(aDir);

    // Filtrage des plugins par leur extension
    QStringList filters;
    filters << "*.dll" << "*.so";
    pluginDir.setNameFilters(filters);
    foreach(QString file, pluginDir.entryList(QDir::Files))
    {
        PluginLoader::unloadByName(pluginDir.absoluteFilePath(file));
    }
}
}

#endif // PLUGINLOADER_H
