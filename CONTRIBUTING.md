# Compilation

Pour compiler, il faut la lib du logger :

* Elle se situe à [https://gitlab.com/fruits.confits/CuteLogger], un fork de [https://github.com/dept2/CuteLogger]
* Il suffit de construire et installer le projet. Le résultat doit être le suivant :
    * dossier `common` au même niveau que le projet
    * `common/lib/CuteLogger/debug|release`: la lib du logger
    * `common/include/CuteLogger` : les headers pour utiliser le logger, le surcharger si nécessaire.
     